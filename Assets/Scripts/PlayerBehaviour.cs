﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBehaviour : Actor
{
    public WolfBehaviour wolf;

    float horizontalSpeed = 5f;

    public bool _action = false;

    public Transform currentItem;
    public Transform positionItemContainer;

    new void Start()
    {
        base.Start();
        wolf = GameObject.FindObjectOfType<WolfBehaviour>();
        positionItemContainer = transform.FindChild("Model/PositionItemContainer").transform;
        _isGood = false;
    }

    new void Update()
    {
        base.Update();

        if (_isDead == false)
        {
            Movement();
        }
    }

    void Movement()
    {
        _animator.SetFloat("Running", Mathf.Abs(Input.GetAxis("Horizontal")));

        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            transform.Translate(Vector2.right * horizontalSpeed * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 0);
            _myState = State.Running;
        }
        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            transform.Translate(Vector2.right * horizontalSpeed * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 180);
            _myState = State.Running;
        }
        if (Input.GetAxisRaw("Horizontal") == 0f && (_myState != State.Idle || _myState != State.Attacking))
        {
            //transform.Translate(Vector2.zero);
            //_animator.SetFloat("Running", 0);
            //_animator.SetTrigger("Idle");
            //ChangeState(State.Idle);
        }

        if (_inFight)
        {
            if (Input.GetButtonDown("Fire1") && _attacking == false)
            {
                Attack();
            }
        }
        else if (_action || positionItemContainer.childCount > 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Action();
            }
        }
    }

    void Attack()
    {
        _attacking = true;
        ChangeState(State.Attacking);
    }

    IEnumerator ActionTimeOut()
    {
        ChangeState(State.Action);
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length / 2);
        if (positionItemContainer.childCount == 0)
        {
            currentItem.GetComponent<Animator>().enabled = false;
            currentItem.GetComponent<BoxCollider2D>().enabled = false;
            currentItem.transform.SetParent(positionItemContainer, false);
            currentItem.localPosition = Vector3.zero;
            currentItem.localScale = currentItem.localScale * 2.1f;
            //currentItem.GetComponent<Animator>().SetBool("Stop", true);
        }
        else
        {
            if (Vector3.Distance(transform.position, wolf.transform.position) <= 1f)
            {
                Destroy(positionItemContainer.GetChild(0).gameObject, 1f);
                wolf.TakeItem();
                _action = false;
            }
            else
            {
                positionItemContainer.GetChild(0).GetComponent<BoxCollider2D>().enabled = true;
                Transform item = positionItemContainer.GetChild(0);
                item.SetParent(null, false);
                item.position = positionItemContainer.position;
                currentItem.localScale = currentItem.localScale / 2.1f;
            }
        }
    }

    void Action()
    {
        StartCoroutine(ActionTimeOut());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Fight")
        {
            _inFight = true;
        }
        if (other.tag == "Item")
        {
            currentItem = other.gameObject.transform;
            _action = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Fight")
        {
            _inFight = false;
        }
        if (other.tag == "Item")
        {
            _action = false;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            if (_attacking == false)
            {
                TakeDamage(1);
            }
        }
    }
}