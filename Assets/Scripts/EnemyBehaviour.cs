﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : Actor
{

    public float minTimeAttacking = 2f, maxTimeAttacking = 4f;

    new void Start()
    {
        base.Start();
        _inFight = true;
        _isGood = true;

        if (!_isDead)
            StartCoroutine(AttackRoutine());
    }

    new void Update()
    {
        base.Update();
        if (_isDead)
        {
            StopCoroutine(AttackRoutine());
        }
    }

    IEnumerator AttackRoutine()
    {
        while (_inFight)
        {
            yield return new WaitForSeconds(Random.Range(minTimeAttacking, maxTimeAttacking));
       
            ChangeState(State.Attacking);
            yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
           
            ChangeState(State.Idle);
        }        
    }

    public void Stun()
    {
        StopCoroutine(AttackRoutine());
        StartCoroutine(StunTimeOut());
    }

    IEnumerator StunTimeOut()
    {
        ChangeState(State.Stun);
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        ChangeState(State.Idle);
        StartCoroutine(AttackRoutine());
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (col.gameObject.GetComponentInParent<PlayerBehaviour>()._attacking)
            {
                TakeDamage(1);
            }
        }
    }
}