﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour
{
    public int _life = 3;
    protected bool _isDead = false;
    protected bool _inFight = false;
    public bool _attacking = false;
    public State _myState = State.Idle;
    protected Animator _animator;
    public bool _isGood;

    private bool _takingDamage;
    protected void Start()
    {
        _animator = transform.GetComponent<Animator>();
        if (_animator == null)
        {
            _animator = GetComponentInChildren<Animator>();
        }
    }

    protected void Update()
    {
        CheckDeath();
    }

    protected void TakeDamage(int value)
    {
        if (_takingDamage == false)
        {
            _takingDamage = true;

            _life -= value;
            if (GetComponent<AudioSource>() != null)
            {
                if (GetComponent<AudioSource>().clip != null)
                {
                    GetComponent<AudioSource>().Play();
                }
            }

            StartCoroutine(TakeDamageCoolDown());
        }
    }
    IEnumerator TakeDamageCoolDown()
    {
        yield return new WaitForSeconds(0.3f);
        _takingDamage = false;
    }

    protected void CheckDeath()
    {
        if (_life <= 0 && _isDead == false)
        {
            _isDead = true;
            _inFight = false;
            ChangeState(State.Dead);
            GameManager.Instance.EndGame(_isGood);
        }
    }

    public State GetState()
    {
        return _myState;
    }

    protected void ChangeState(State newState)
    {
        _myState = newState;
        ChangeSprite();
    }

    void ChangeSprite()
    {
        switch (_myState)
        {
            case State.Idle:
                _animator.SetTrigger("Idle");
                break;
            case State.Running:
                _animator.SetTrigger("Running");
                break;
            case State.Action:
                _animator.SetTrigger("Action");
                break;
            case State.Attacking:
                _animator.SetTrigger("Attacking");
                break;
            case State.Stun:
                _animator.SetTrigger("Stun");
                break;
            case State.Dead:
                _animator.SetTrigger("Dead");
                break;
            default:
                _animator.SetTrigger("Idle");
                break;
        }
    }
}