﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

[RequireComponent(typeof(AudioSource))]
public class PlayVideo : MonoBehaviour
{
    public int netxScene;
#if UNITY_EDITOR && UNITY_STANDALONE
    public MovieTexture video;
    public AudioSource audio;

    void Start()
    {
        
        GetComponent<RawImage>().texture = video as MovieTexture;
        video.Play();
        audio = GetComponent<AudioSource>();
        audio.clip = video.audioClip;
        
        audio.Play();

        StartCoroutine(WaitVideoEnd(video.duration));
    }

    //IEnumerator WaitVideoEnd()
    //{
    //    yield return new WaitForSeconds(video.duration);
    //    SceneManager.LoadScene(netxScene);
    //}

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (video.isPlaying)
            {
                video.Pause();
            }
            else
            {
                video.Play();
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            video.Stop();
            SceneManager.LoadScene(netxScene);
        }
    }
#endif
#if UNITY_WEBGL
    WebGLMovieTexture _webglTex;
    public int _wichCutscene = 1;
    void Start()
    {
        _webglTex = new WebGLMovieTexture("StreamingAssets/Cutscene_" + _wichCutscene + ".mp4");
        GetComponent<RawImage>().texture = _webglTex;
        _webglTex.Play();

        StartCoroutine(WaitVideoEnd(_webglTex.duration));
    }
    void Update()
    {
        _webglTex.Update();
    }
#endif

    IEnumerator WaitVideoEnd(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene(netxScene);
    }
}