﻿public enum State
{
    Idle,
    Running,
    Action,
    Attacking,
    Stun,
    Dead
}