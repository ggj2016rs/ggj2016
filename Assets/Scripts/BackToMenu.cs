﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Back());
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    IEnumerator Back()
    {
        yield return new WaitForSeconds(10f);
        SceneManager.LoadScene("Menu");
    }
}
