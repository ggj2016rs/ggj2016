﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using God;
using System;

public class GameManager : Singleton<GameManager>
{


    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Scene1")
        {
            GameObject.FindGameObjectWithTag("Player").transform.localScale = new Vector3(0.7f, 0.7f, 1f);
            GameObject.FindGameObjectWithTag("Wolf").transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        }
        else if (SceneManager.GetActiveScene().name == "Scene2")
        {
            GameObject.FindGameObjectWithTag("Player").transform.localScale = new Vector3(0.9f, 0.9f, 1f);
            GameObject.FindGameObjectWithTag("Wolf").transform.localScale = new Vector3(0.9f, 0.9f, 1f);
        }
        else if (SceneManager.GetActiveScene().name == "Scene3")
        {
            GameObject.FindGameObjectWithTag("Player").transform.localScale = new Vector3(1.1f, 1.1f, 1f);
            GameObject.FindGameObjectWithTag("Wolf").transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        }
    }

    void Update()
    {

    }

    public void _PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void _ResumeGame()
    {
        Time.timeScale = 1f;
    }

    public void _RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void _LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void EndGame(bool isGood)
    {
        StartCoroutine(WaitToEnd(isGood));
    }

    public void _LoadScene(String scene)
    {
        SceneManager.LoadScene(scene);
    }

    IEnumerator WaitToEnd(bool isGood)
    {
        yield return new WaitForSeconds(1f);
        Debug.Log(isGood);
        if (isGood)
        {
            if (SceneManager.GetActiveScene().buildIndex < 5)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else
            {
                _LoadMenu();
            }
        }
        else
        {
            _RestartGame();
        }
    }
}
