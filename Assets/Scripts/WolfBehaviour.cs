﻿using UnityEngine;
using System.Collections;

public class WolfBehaviour : Actor
{
    public Transform player;

    public float currentDistance, distanceBetweenPlayer = 2f;
    public float speedMovimentation = 5f;

    public float minTimeAttacking = 2f, maxTimeAttacking = 4f;

    public EnemyBehaviour enemy;

    new void Start()
    {
        base.Start();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    new void Update()
    {
        base.Update();

        if (_isDead == false && _inFight == false)
        {
            FollowPlayer();
        }

        IdentifyDirection();
    }

    void IdentifyDirection()
    {
        
        if (player.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector2(0, 180);
            _myState = State.Running;
        }
        else if (player.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector2(0, 0);
            _myState = State.Running;
        }
    }

    void FollowPlayer()
    {
        currentDistance = Vector3.Distance(transform.position, player.transform.position);

        if (currentDistance > distanceBetweenPlayer)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speedMovimentation * Time.deltaTime);
            ChangeState(State.Running);
        }
        else if (_myState != State.Idle)
            ChangeState(State.Idle);
        
    }

    public void TakeItem()
    {
        GameManager.Instance.EndGame(true);
    }

    void Attack()
    {
        _attacking = true;
        AttackTimeOut();
        ChangeState(State.Attacking);
    }

    IEnumerator AttackTimeOut()
    {
        while (_inFight)
        {
            yield return new WaitForSeconds(Random.Range(minTimeAttacking, maxTimeAttacking));
            ChangeState(State.Attacking);

            yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
            enemy.Stun();
            ChangeState(State.Idle);
        }  
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Fight")
        {
            _inFight = true;
            ChangeState(State.Idle);
            enemy = GameObject.FindObjectOfType<EnemyBehaviour>();
            StartCoroutine(AttackTimeOut());
        }
    }
}